var mongodb = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3977;

mongodb.connect('mongodb://127.0.0.1:27017/proyecto', (error,res)=>{
    if (error) {
        throw error;
    }else{
        console.warn("Conexión Exitosa");
        app.listen(port, function(){
            console.log("El servidor de api rest escuchando en http://localhost: " + port);
        });
    }
});
